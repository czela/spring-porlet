/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.czela.example.springportlet.serviceimpl;

import com.czela.example.springportlet.service.WeatherService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 *
 * @author czela
 */
@Service
public class WeatherServiceImpl implements WeatherService{

    public Map<String, Double> getMajorCityTemperatures() {
        Map<String,Double> temperatures = new HashMap<String, Double>();
        temperatures.put("Lima", 12.5);
        temperatures.put("Ayacucho", 23.6);
        temperatures.put("Apurimac", 10.9);
        return temperatures;
    }

}
