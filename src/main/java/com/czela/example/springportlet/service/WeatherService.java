/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.czela.example.springportlet.service;

import java.util.Map;

/**
 *
 * @author czela
 */
public interface WeatherService {
    public Map<String, Double> getMajorCityTemperatures();
}
