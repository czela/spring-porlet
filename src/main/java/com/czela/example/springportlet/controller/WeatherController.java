/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.czela.example.springportlet.controller;

import com.czela.example.springportlet.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author czela
 */
@Controller
@RequestMapping("VIEW")
public class WeatherController {
    
    @Autowired
    private WeatherService weatherService;

    @RequestMapping
    public String handleRenderRequestInternal(Model model) throws Exception{
        model.addAttribute("temperatures", weatherService.getMajorCityTemperatures());
        return "weatherView";
    }
}
