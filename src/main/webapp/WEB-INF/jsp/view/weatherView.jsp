<%-- 
    Document   : weatherView
    Created on : 31/07/2012, 11:28:39 AM
    Author     : czela
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table border="1">
    <tr>
        <td>Ciudad</td>
        <td>Temperatura</td>
    </tr>
    <c:forEach items="${temperatures}" var="temperature">
        <tr>
            <td>${temperature.key}</td>
            <td>${temperature.value}</td>
        </tr>
    </c:forEach>
</table>
